<?php 
	require __DIR__ .'/../main/Calculator.php';

	class CalculatorTest extends PHPUnit\Framework\TestCase {
		private $Calculator;

		protected function setUp() {
			$this->calculator = new Calculator(12, 6);
		}

		public function testAdd() {
			$result = $this->calculator->add();
			$this->assertEquals(18, $result);
		}

		public function testSub() {
			$result = $this->calculator->sub();
			$this->assertEquals(6, $result);
		}

		public function testMul() {
			$result = $this->calculator->mul();
			$this->assertEquals(72, $result);
		}

		protected function tearDown() {
			$this->calculator = NULL;
		}
	}
?>
