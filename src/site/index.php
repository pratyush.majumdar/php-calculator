<!DOCTYPE html>
<html lang="en">
	<head>
		<title>PHP Calculator</title>
		<meta name="viewport" content="width=device-width, initial-scale=1"/>
	</head>

	<body>
		<?php
			$answer = 0;
			if(isset($_POST['submit'])) {
				$num1 = $_POST["num1"];
				$num2 = $_POST["num2"];
				$operation = $_POST["operation"];

				$mycalc = new Calculator($num1, $num2);
				switch ($operation) {
					case 'add':
						$answer = $mycalc->add();
						break;
					case 'sub':
						$answer = $mycalc->sub();
						break;
					case 'mul':
						$answer = $mycalc->mul();
						break;
					case 'div':
						$answer = $mycalc->div();
						break;
					default:
    						$answer = 0;
    						break;
          		}
			}
		?>
		
		<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			Number 1 <input type="text" value="num1" name="num1"> <br/>
			Number 2 <input type="text" value="num2" name="num2"> <br/>
			<select id="operation" name="operation">
				<option value="add">Addition</option>
				<option value="sub">Substraction</option>
				<option value="mul">Multiplication</option>
				<option value="div">Division</option>
			</select>
			<input type="Submit" value="Submit">
			<h1>Answer: <?php echo $answer; ?></h1>
		</form>
	</body>
</html>
