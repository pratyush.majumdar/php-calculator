<?php
	class Calculator {
		private $num1;
		private $num2;

		public function __construct(string $num1, int $num2) {
	        $this->num1 = $num1;
	        $this->num2 = $num2;
    	}

    	public function add() {
			return $this->num1 + $this->num2;
		}

		public function sub() {
			return $this->num1 - $this->num2;
		}

		public function mul() {
			return $this->num1 * $this->num2;
		}

		public function div() {
			return $this->num1 / $this->num2;
		}
	}
?>